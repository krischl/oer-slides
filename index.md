---
title: My Slide Decks
---

## https://axel-klinger.gitlab.io/my-slides/

- [Einen Kurs mit GitLab erstellen](./oer-ws-1.html) (13./14. Juni 2019)
- [Einen Jahrgang/Studiengang mit GitLab abbilden](./oer-ws-2.html) (13./14. Juni 2019)
- [Ein Beispiel](./oer-example.html) (13./14. Juni 2019)
- [Ein VSC Beispiel](./test.html) (13./14. Juni 2019)
